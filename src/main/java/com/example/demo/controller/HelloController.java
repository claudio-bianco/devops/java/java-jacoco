package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class HelloController {
    
    @GetMapping(value="/")
    public String getHello() {
        return "Hello World!";
    }

    @GetMapping(value="/name")
    public String getName(@RequestParam String name){
        return String.format("Hello, %s", name);
    }

    @GetMapping(value="/value")
    public int getValue(){
        return 10;
    }

    @GetMapping(value="/sum")
    public int getSum(@RequestParam int num){
        return num + 10;
    }

    @GetMapping(value="/palin")
    public boolean isPalindrome(String inputString) {
        if (inputString.length() == 0) {
            return true;
        } else {
            char firstChar = inputString.charAt(0);
            char lastChar = inputString.charAt(inputString.length() - 1);
            String mid = inputString.substring(1, inputString.length() - 1);
            return (firstChar == lastChar) && isPalindrome(mid);
        }
    }

    @GetMapping(value="/message")
    public String getMessage(String name)
    {
        StringBuilder s = new StringBuilder();
        if(name == null || name.trim().length() == 0)
        {
            s = s.append("Please Provide a name!");
        }
        else
        {
            s.append("Hello " + name + "!");
        }
        return s.toString();
    }    

}