package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.controller.HelloController;

@SpringBootTest
class DemoApplicationTests {

	@Test
	void testHello() {
		HelloController hello = new HelloController();      // Arrange
		String response = hello.getHello();                 // Act
		assertEquals("Hello World!", response);   // Assert
	}

    @Test
    void testName() {
        HelloController controller = new HelloController();    // Arrange
        String response = controller.getName("Claudio");  // Act
        assertEquals("Hello, Claudio", response);     // Assert
    }

    @Test
    void testValue() {
        HelloController controller = new HelloController();    // Arrange        
        assertEquals(10, controller.getValue());      // Assert
    }

    @Test
    void testSum() {
        HelloController controller = new HelloController();      // Arrange
        assertEquals(20, controller.getSum(10));   // Assert
    }

    @Test
    void whenEmptyString_thenAccept() {
        HelloController controller = new HelloController();
        assertTrue(controller.isPalindrome(""));
    }

    @Test
    void whenPalindrom_thenAccept() {
        HelloController controller = new HelloController();
        assertTrue(controller.isPalindrome("noon"));
    }
        
    @Test
    void whenNearPalindrom_thanReject(){
        HelloController controller = new HelloController();
        assertFalse(controller.isPalindrome("neon"));
    }

    @Test
    void testNameDailyCodeBuffer()
    {
        HelloController controller = new HelloController();
        assertEquals("Hello Daily Code Buffer!", controller.getMessage("Daily Code Buffer"));
    }

    @Test
    void testNameBlank()
    {
        HelloController controller = new HelloController();
        assertEquals("Please Provide a name!", controller.getMessage(""));
    }

    @Test
    void testNameNull()
    {
        HelloController controller = new HelloController();
        assertEquals("Please Provide a name!", controller.getMessage(null));
    }
    
}